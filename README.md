# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* To explore emergent clusters and/or groupings in Payscale data
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Make sure you have R installed on your machine, preferably along with R Studio
* Dependencies- The Package biclust (https://cran.r-project.org/web/packages/biclust/biclust.pdf)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jay Parchure: Jay.Parchure@act.org
* OFI POC with PayScale: Laura.Frisby@act.org